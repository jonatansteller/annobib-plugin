( function( wp ) {

  // IMPORT NECESSARY FUNCTIONS (PACKAGES NEED TO BE ENQUEUED ALONG WITH THIS SCRIPT)
  var __ = wp.i18n.__;
  var el = wp.element.createElement;
  var registerBlockType = wp.blocks.registerBlockType;
  var TextControl = wp.components.TextControl;
  var TextareaControl = wp.components.TextareaControl;
  var SelectControl = wp.components.SelectControl;
  var CheckboxControl = wp.components.CheckboxControl;
  var RangeControl = wp.components.RangeControl;
  var RichText = wp.blockEditor.RichText;

  // REGISTERING THE YEAR OF FIRST PUBLICATION BLOCK
  registerBlockType( 'annobib/first-publication', {
    title: __( 'First Publication', 'annobib-plugin' ),
    description: __( 'Block containing the year the text was first published in.', 'annobib-plugin' ),
    icon: 'calendar-alt',
    category: 'annobib',
    keywords: [ __( 'year', 'annobib-plugin' ), __( 'publication', 'annobib-plugin' ), __( 'published', 'annobib-plugin' ) ],
    attributes: {
      year: {
        type: 'integer',
        source: 'meta',
        meta: 'annobib-meta-year'
      },
      yearGuess: {
        type: 'boolean',
        source: 'meta',
        meta: 'annobib-meta-year-guess'
        }
    },
    example: {
      attributes: {
        year: 1973,
        yearGuess: true
      },
    },

    // What happens when you edit the block
    edit: function( props ) {
      var className = props.className;
      var setAttributes = props.setAttributes;

      function updateBlockValueYear( year ) {
        setAttributes({ year });
      }

      function updateBlockValueYearGuess( yearGuess ) {
        setAttributes({ yearGuess });
      }

      return el( 'div', {
          className: className
        },
        el( 'h2', {}, __( 'First Publication', 'annobib-plugin' ) ),
        el( RangeControl, {
          label: __( 'Year of first publication', 'annobib-plugin' ),
          value: props.attributes.year,
          min: -1000,
          max: 2050,
          allowReset: false,
          beforeIcon: 'calendar-alt',
          onChange: updateBlockValueYear
        } ),
        el( CheckboxControl, {
          label: __( 'Approximate date (c.)', 'annobib-plugin' ),
          checked: props.attributes.yearGuess,
          onChange: updateBlockValueYearGuess
        } )
      );
    },

    // Do not save information to the block because it is automatically saved in meta
    save: function() {
      return null;
    }
  } );


  // REGISTERING THE RECOMMENDED EDITION BLOCK
  registerBlockType( 'annobib/recommended-edition', {
    title: __( 'Critical Edition', 'annobib-plugin' ),
    description: __( 'Block providing a critical edition of the text.', 'annobib-plugin' ),
    icon: 'format-quote',
    category: 'annobib',
    keywords: [ __( 'citation', 'annobib-plugin' ), __( 'quote', 'annobib-plugin' ), __( 'edition', 'annobib-plugin' ), __( 'length', 'annobib-plugin' ), __( 'identifier', 'annobib-plugin' ), __( 'rating', 'annobib-plugin' ), __( 'recommendation', 'annobib-plugin' ), __( 'recommended', 'annobib-plugin' ), __( 'critical', 'annobib-plugin' ) ],
    attributes: {
      citation: {
        type: 'string',
        source: 'meta',
        meta: 'annobib-meta-citation'
      },
      uniqueidType: {
        type: 'string',
        source: 'meta',
        meta: 'annobib-meta-uniqueid-type'
      },
      uniqueidID: {
        type: 'string',
        source: 'meta',
        meta: 'annobib-meta-uniqueid-id'
      },
      contentrating: {
        type: 'string',
        source: 'meta',
        meta: 'annobib-meta-contentrating'
      },
      lengthAmount: {
        type: 'integer',
        source: 'meta',
        meta: 'annobib-meta-length-amount'
      },
      lengthUnit: {
        type: 'string',
        source: 'meta',
        meta: 'annobib-meta-length-unit'
      }
    },
    example: {
      attributes: {
        citation: __('Kipling, Rudyard. “The Jungle Book.” The Jungle Books, edited by W. W. Robson, Oxford World\'s Classics, Oxford UP, 2008, pp. 1–143.', 'annobib-plugin' ),
        uniqueidType: __('ISBN', 'annobib-plugin' ),
        uniqueidID: '9780199536450',
        contentrating: __('FSK 12', 'annobib-plugin' ),
        lengthAmount: 143,
        lengthUnit: __('pages', 'annobib-plugin' )
      },
    },

    // What happens when you edit the block
    edit: function( props ) {
      var className = props.className;
      var setAttributes = props.setAttributes;

      function updateBlockValueCitation( citation ) {
        setAttributes({ citation });
      }

      function updateBlockValueUniqueIDType( uniqueidType ) {
        setAttributes({ uniqueidType });
      }

      function updateBlockValueUniqueIDID( uniqueidID ) {
        setAttributes({ uniqueidID });
      }

      function updateBlockValueContentRating( contentrating ) {
        setAttributes({ contentrating });
      }

      function updateBlockValueLengthAmount( lengthAmount ) {
        setAttributes({ lengthAmount });
      }

      function updateBlockValueLengthUnit( lengthUnit ) {
        setAttributes({ lengthUnit });
      }

      return el( 'div', {
          className: className
        },
        el( 'h2', {}, __( 'Critical Edition', 'annobib-plugin' ) ),
        el( RichText, {
          className: 'annobib-citation',
          placeholder: __( 'Add full citation', 'annobib-plugin' ),
          tagName: 'p',
          keepPlaceholderOnFocus: false,
          allowedFormats: [ 'core/bold', 'core/italic', 'core/link' ],
          value: props.attributes.citation,
          onChange: updateBlockValueCitation
        } ),
        el( 'div', {
          className: 'annobib-container'
        },
          el( SelectControl, {
            className: 'annobib-container-narrow',
            label: __( 'Type of identifier', 'annobib-plugin' ),
            value: props.attributes.uniqueidType,
            options: [
              { label: '', value: false },
              { label: __( 'ISBN', 'annobib-plugin' ), value: 'ISBN' },
              { label: __( 'DOI', 'annobib-plugin' ), value: 'DOI' },
              { label: __( 'ISSN', 'annobib-plugin' ), value: 'ISSN' },
              { label: __( 'Web address', 'annobib-plugin' ), value: 'URL' }
            ],
            onChange: updateBlockValueUniqueIDType
          } ),
          el( TextControl, {
            className: 'annobib-container-wide',
            label: __( 'Unique identifier', 'annobib-plugin' ),
            value: props.attributes.uniqueidID,
            onChange: updateBlockValueUniqueIDID
          } ),
        ),
        el( 'div', {
          className: 'annobib-entry'
        },
          el( SelectControl, {
            label: __( 'German content rating', 'annobib-plugin' ),
            value: props.attributes.contentrating,
            options: [
              { label: '', value: false },
              { label: __( 'FSK 0', 'annobib-plugin' ), value: 'FSK 0' },
              { label: __( 'FSK 6', 'annobib-plugin' ), value: 'FSK 6' },
              { label: __( 'FSK 12', 'annobib-plugin' ), value: 'FSK 12' },
              { label: __( 'FSK 16', 'annobib-plugin' ), value: 'FSK 16' },
              { label: __( 'FSK 18', 'annobib-plugin' ), value: 'FSK 18' },
              { label: __( 'USK 0', 'annobib-plugin' ), value: 'USK 0' },
              { label: __( 'USK 6', 'annobib-plugin' ), value: 'USK 6' },
              { label: __( 'USK 12', 'annobib-plugin' ), value: 'USK 12' },
              { label: __( 'USK 16', 'annobib-plugin' ), value: 'USK 16' },
              { label: __( 'USK 18', 'annobib-plugin' ), value: 'USK 18' }
            ],
            onChange: updateBlockValueContentRating
          } ),
        ),
        el( 'div', {
          className: 'annobib-container annobib-container-last'
        },
          el( RangeControl, {
            className: 'annobib-container-wide',
            label: __( 'Length', 'annobib-plugin' ),
            value: props.attributes.lengthAmount,
            min: 0,
            max: 1500,
            allowReset: false,
            beforeIcon: 'admin-page',
            onChange: updateBlockValueLengthAmount
          } ),
          el( SelectControl, {
            className: 'annobib-container-narrow',
            label: __( 'Unit', 'annobib-plugin' ),
            value: props.attributes.lengthUnit,
            options: [
              { label: '', value: false },
              { label: __( 'pages', 'annobib-plugin' ), value: 'pages' },
              { label: __( 'lines', 'annobib-plugin' ), value: 'lines' },
              { label: __( 'minutes', 'annobib-plugin' ), value: 'minutes' }
            ],
            onChange: updateBlockValueLengthUnit
          } )
        ),
      );
    },

    // Do not save information to the block because it is automatically saved in meta
    save: function() {
      return null;
    }
  } );


  // REGISTERING THE USEFUL LINKS BLOCK
  registerBlockType( 'annobib/useful-links', {
    title: __( 'Useful Links', 'annobib-plugin' ),
    description: __( 'Block providing useful links, e.g. to study guides or editions in the public domain.', 'annobib-plugin' ),
    icon: 'admin-links',
    category: 'annobib',
    keywords: [ __( 'links', 'annobib-plugin' ), __( 'study guide', 'annobib-plugin' ), __( 'edition', 'annobib-plugin' ), __( 'public domain', 'annobib-plugin' ), __( 'useful', 'annobib-plugin' ) ],
    attributes: {
      usefullinks: {
        type: 'string',
        source: 'meta',
        meta: 'annobib-meta-usefullinks'
      }
    },
    example: {
      attributes: {
        usefullinks: '<ul><li><a href="https://archive.org/details/nightofthelivingdeaddvd">Night of the Living Dead at the Internet Archive</a></li><li><a href="https://archive.org/details/nightofthelivingdeaddvd">Night of the Living Dead at the Internet Archive</a></li></ul>'
      },
    },

    // What happens when you edit the block
    edit: function( props ) {
      var className = props.className;
      var setAttributes = props.setAttributes;

      function updateBlockValueUsefulLinks( usefullinks ) {
        setAttributes({ usefullinks });
      }

      return el( 'div', {
          className: className
        },
        el( 'h2', {}, __( 'Useful Links', 'annobib-plugin' ) ),
        el( RichText, {
          className: 'annobib-usefullinks',
          placeholder: __( 'Add useful links', 'annobib-plugin' ),
          tagName: 'ul',
          multiline: 'li',
          keepPlaceholderOnFocus: false,
          allowedFormats: [ 'core/bold', 'core/italic', 'core/link' ],
          value: props.attributes.usefullinks,
          onChange: updateBlockValueUsefulLinks
        } ),
      );
    },

    // Do not save information to the block because it is automatically saved in meta
    save: function() {
      return null;
    }
  } );


  // REGISTERING THE NOTES FOR EDITORS BLOCK
  registerBlockType( 'annobib/for-editors', {
    title: __( 'What the Editors Should Know', 'annobib-plugin' ),
    description: __( 'Block containing notes for the editoral process.', 'annobib-plugin' ),
    icon: 'text',
    category: 'annobib',
    keywords: [ __( 'notes', 'annobib-plugin' ), __( 'editorial', 'annobib-plugin' ), __( 'editor', 'annobib-plugin' ) ],
    attributes: {
      notes: {
        type: 'string',
        source: 'meta',
        meta: 'annobib-meta-notes',
      },
      featured: {
        type: 'boolean',
        source: 'meta',
        meta: 'annobib-meta-featured',
      }
    },
    example: {
      attributes: {
        notes: __('Not sure if this was originally published in 1973.', 'annobib-plugin' ),
        featured: true,
      }
    },

    // What happens when you edit the block
    edit: function( props ) {
      var className = props.className;
      var setAttributes = props.setAttributes;

      function updateBlockValueFeatured( featured ) {
        setAttributes({ featured });
      }

      function updateBlockValueNotes( notes ) {
        setAttributes({ notes });
      }

      return el( 'div', {
          className: className
        },
        el( 'h2', {}, __( 'For the Editors', 'annobib-plugin' ) ),
        el( TextareaControl, {
          label: __( 'Notes (not published)', 'annobib-plugin' ),
          value: props.attributes.notes,
          onChange: updateBlockValueNotes
        } ),
        el( CheckboxControl, {
          label: __( 'Feature this entry on the front page', 'annobib-plugin' ),
          checked: props.attributes.featured,
          onChange: updateBlockValueFeatured
        } )
      );
    },

    // Do not save information to the block because it is automatically saved in meta
    save: function() {
      return null;
    }
  } );
} )( window.wp );
