<?php
/*
Plugin Name: Annotated Bibliography
Plugin URI: https://gitlab.com/jonatansteller/annobib-plugin
Description: Adds a custom post type and custom taxonomy to build a digitally annotated bibliography.
Version: 0.9
Requires at least: 5.2
Requires PHP: 7.0
Author: Jonatan Jalle Steller
Author URI: https://gitlab.com/jonatansteller
Text Domain: annobib-plugin
Domain Path: /languages
License: GPL3 or later
License URI: https://www.gnu.org/licenses/gpl-3.0.html

Annotated Bibliography is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or (at your option)
any later version.

This WordPress plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this plugin. If not, see gnu.org/licenses/gpl-3.0.html.
*/


// CHECK IF ACTIVATED WITHIN A WORDPRESS ENVIRONMENT
defined( 'ABSPATH' ) or die( __( 'Ah ah ah. You did not say the magic word!', 'annobib-plugin' ) );


// ACTIVATION ROUTINE
function annobib_activation() {

	// Register the new post type
	annobib_setup_post_type();

	// Clear permalinks to add the new post type
	flush_rewrite_rules();

	// Add custom roles
	annobib_setup_roles();
}
register_activation_hook( __FILE__, 'annobib_activation' );


// DEACTIVATION ROUTINE
function annobib_deactivation() {

	// Unregister the old post type including taxonomies and meta fields
	unregister_post_type( 'bibliography-en' );
	unregister_post_type( 'bibliography-de' );

	// Clear permalinks to remove the old post type
	flush_rewrite_rules();

	// Remove custom roles
	remove_role( 'super_author' );
}
register_deactivation_hook( __FILE__, 'annobib_deactivation' );


// LOAD TRANSLATION FILES
function annobib_setup_translation() {
	load_plugin_textdomain( 'annobib-plugin', FALSE, basename( dirname( __FILE__ ) ) . '/languages' );
}
add_action( 'plugins_loaded', 'annobib_setup_translation' );


// ADD BIBLIOGRAPHY POST TYPE
function annobib_setup_post_type() {

	// Bibliography: labels
	$labels = array(
		'singular_name'						 => __( 'Entry', 'annobib-plugin' ),
		'add_new'									 => __( 'Add New', 'annobib-plugin' ),
		'add_new_item'						 => __( 'Add New Entry', 'annobib-plugin' ),
		'edit_item'								 => __( 'Edit Entry', 'annobib-plugin' ),
		'new_item'								 => __( 'New Entry', 'annobib-plugin' ),
		'view_item'								 => __( 'View Entry', 'annobib-plugin' ),
		'view_items'							 => __( 'View Entries', 'annobib-plugin' ),
		'search_items'						 => __( 'Search Entries', 'annobib-plugin' ),
		'not_found'								 => __( 'No entries found.', 'annobib-plugin' ),
		'not_found_in_trash'			 => __( 'No entries found in Trash.', 'annobib-plugin' ),
		'all_items'								 => __( 'All Entries', 'annobib-plugin' ),
		'archives'								 => __( 'Bibliography', 'annobib-plugin' ),
		'attributes'						   => __( 'Entry Attributes', 'annobib-plugin' ),
		'insert_into_item'			 	 => __( 'Insert into summary', 'annobib-plugin' ),
		'uploaded_to_this_item' 	 => __( 'Uploaded to this entry', 'annobib-plugin' ),
		'featured_image'					 => __( 'Featured Image', 'annobib-plugin' ),
		'set_featured_image'			 => __( 'Set featured image', 'annobib-plugin' ),
		'remove_featured_image'		 => __( 'Remove featured image', 'annobib-plugin' ),
		'use_featured_image'			 => __( 'Use as featured image', 'annobib-plugin' ),
		'filter_items_list'				 => __( 'Filter bibliography', 'annobib-plugin' ),
		'items_list_navigation'		 => __( 'Bibliography navigation', 'annobib-plugin' ),
		'items_list'							 => __( 'Entry list', 'annobib-plugin' ),
		'item_published'					 => __( 'Entry published.', 'annobib-plugin' ),
		'item_published_privately' => __( 'Entry published privately.', 'annobib-plugin' ),
		'item_reverted_to_draft'	 => __( 'Entry reverted to draft.', 'annobib-plugin' ),
		'item_scheduled'					 => __( 'Entry scheduled.', 'annobib-plugin' ),
		'item_updated'						 => __( 'Entry updated.', 'annobib-plugin' )
	);

	// Bibliography: labels by subject
	$labelsEN = array(
		'name' => __( 'Bibliography (English)', 'annobib-plugin' )
	);
	$labelsDE = array(
		'name' => __( 'Bibliography (German)', 'annobib-plugin' )
	);
	$labelsEN = array_merge($labelsEN, $labels);
	$labelsDE = array_merge($labelsDE, $labels);

	// Bibliography: configuration
	$args = array(
		'description'			 => __( 'Bibliographic entries including summaries and other annotations.', 'annobib-plugin' ),
		'public'					 => true,
		'hierarchical'		 => false,
		'show_in_rest'		 => true,
		'menu_position'		 => null,
		'menu_icon'				 => 'dashicons-book',
		'capability_type'  => 'post',
		'map_meta_cap'		 => true,
		'supports'				 => array(
			'title',
			'editor',
			'comments',
			'revisions',
			'trackbacks',
			'thumbnail',
			'custom-fields'
		),
		'has_archive'			 => true,
		'can_export'			 => true,
		'delete_with_user' => false,
		'template'				 => array(
			array( 'core/paragraph', array(
				'placeholder'	 => __( 'Add summary; optionally add notes on structure, style, complexity, or classroom activities in a second paragraph', 'annobib-plugin' ),
			) ),
			array( 'annobib/first-publication', array() ),
			array( 'annobib/recommended-edition', array() ),
			array( 'annobib/useful-links', array() ),
			array( 'annobib/for-editors', array() )
		),
		//'template_lock'  => 'all' // Causes a JS error when pasting content in an empty paragraph block; RichText fields remain unaffected by this
	);

	// Bibliography: configuration by subject
	$argsEN = array(
		'labels'  => $labelsEN,
		'rewrite' => array( 'slug' => 'en' )
	);
	$argsDE = array(
		'labels'  => $labelsDE,
		'rewrite' => array( 'slug' => 'de' )
	);
	$argsEN = array_merge($argsEN, $args);
	$argsDE = array_merge($argsDE, $args);

  // Bibliography-EN and Bibliography-DE
	register_post_type( 'bibliography-en', $argsEN );
	register_post_type( 'bibliography-de', $argsDE );
}
add_action( 'init', 'annobib_setup_post_type' );


// ADD TAXONOMIES TO BIBLIOGRAPHY POST TYPE
function annobib_setup_custom_taxonomy() {

  // Genres: labels
	$labels = array(
		'name'									=> __( 'Genres', 'annobib-plugin' ),
		'singular_name'					=> __( 'Genre', 'annobib-plugin' ),
		'search_items'					=> __( 'Search Genres', 'annobib-plugin' ),
		'all_items'							=> __( 'All Genres', 'annobib-plugin' ),
		'parent_item'						=> __( 'Superordinate Genre', 'annobib-plugin' ),
		'parent_item_colon'			=> __( 'Superordinate Genre:', 'annobib-plugin' ),
		'edit_item'							=> __( 'Edit Genre', 'annobib-plugin' ),
		'view_item'							=> __( 'View Genre', 'annobib-plugin' ),
		'update_item'						=> __( 'Update Genre', 'annobib-plugin' ),
		'add_new_item'					=> __( 'Add New Genre', 'annobib-plugin' ),
		'new_item_name'					=> __( 'New Genre Name', 'annobib-plugin' ),
		'not_found'							=> __( 'No genres found.', 'annobib-plugin' ),
		'no_terms'							=> __( 'No genres', 'annobib-plugin' ),
		'items_list_navigation' => __( 'Genres list navigation', 'annobib-plugin' ),
		'items_list'						=> __( 'Genres list', 'annobib-plugin' ),
		'most_used'							=> __( 'Most Used', 'annobib-plugin' ),
		'back_to_items'					=> __( '&larr; Back to Genres', 'annobib-plugin' )
	);

  // Genres: configuration
	$args = array(
		'labels'            => $labels,
		'public'            => true,
    'show_in_rest'      => true,
		'show_admin_column' => true,
		'description'       => __( 'Place meta-genres at the top level and drill down to more specific ones if needed.', 'annobib-plugin' ),
  	'hierarchical'      => true,
		'rewrite'           => true
	);

  // Genres
	register_taxonomy( 'en-genre', array( 'bibliography-en' ), $args );
	register_taxonomy( 'de-genre', array( 'bibliography-de' ), $args );

  // Authors: labels
	$labels = array(
		'name'											 => __( 'Authors', 'annobib-plugin' ),
		'singular_name'							 => __( 'Author', 'annobib-plugin' ),
		'search_items'							 => __( 'Search Authors', 'annobib-plugin' ),
		'popular_items'							 => __( 'Popular Authors', 'annobib-plugin' ),
		'all_items'									 => __( 'All Authors', 'annobib-plugin' ),
		'edit_item'									 => __( 'Edit Author', 'annobib-plugin' ),
		'view_item'									 => __( 'View Author', 'annobib-plugin' ),
		'update_item'								 => __( 'Update Author', 'annobib-plugin' ),
		'add_new_item'							 => __( 'Add New Author', 'annobib-plugin' ),
		'new_item_name'							 => __( 'New Author Name', 'annobib-plugin' ),
		'separate_items_with_commas' => __( 'Separate authors with commas', 'annobib-plugin' ),
		'add_or_remove_items'				 => __( 'Add or remove authors', 'annobib-plugin' ),
		'choose_from_most_used'			 => __( 'Choose from the most prolific authors', 'annobib-plugin' ),
		'not_found'									 => __( 'No authors found.', 'annobib-plugin' ),
		'no_terms'									 => __( 'No authors', 'annobib-plugin' ),
		'items_list_navigation' 		 => __( 'Authors list navigation', 'annobib-plugin' ),
		'items_list'								 => __( 'Authors list', 'annobib-plugin' ),
		'most_used'									 => __( 'Most Used', 'annobib-plugin' ),
		'back_to_items'							 => __( '&larr; Back to Authors', 'annobib-plugin' )
	);

  // Authors: configuration
	$args = array(
		'labels'            => $labels,
		'public'            => true,
    'show_in_rest'      => true,
		'show_admin_column' => true,
		'description'       => __( 'Provide the names of all authors of the bibliographic entry.', 'annobib-plugin' ),
  	'hierarchical'      => false,
		'rewrite'           => true
	);

  // Authors
	register_taxonomy( 'en-writer', array( 'bibliography-en' ), $args );
	register_taxonomy( 'de-writer', array( 'bibliography-de' ), $args );

  // Countries of Publication: labels
	$labels = array(
		'name'											 => __( 'Countries of Publication', 'annobib-plugin' ),
		'singular_name'							 => __( 'Country', 'annobib-plugin' ),
		'search_items'							 => __( 'Search Countries', 'annobib-plugin' ),
		'popular_items'							 => __( 'Popular Countries', 'annobib-plugin' ),
		'all_items'									 => __( 'All Countries', 'annobib-plugin' ),
		'edit_item'									 => __( 'Edit Country', 'annobib-plugin' ),
		'view_item'									 => __( 'View Country', 'annobib-plugin' ),
		'update_item'								 => __( 'Update Country', 'annobib-plugin' ),
		'add_new_item'							 => __( 'Add New Country', 'annobib-plugin' ),
		'new_item_name'							 => __( 'New Country Name', 'annobib-plugin' ),
		'separate_items_with_commas' => __( 'Separate countries with commas', 'annobib-plugin' ),
		'add_or_remove_items'				 => __( 'Add or remove countries', 'annobib-plugin' ),
		'choose_from_most_used'			 => __( 'Choose from the most common countries of publication', 'annobib-plugin' ),
		'not_found'									 => __( 'No countries found.', 'annobib-plugin' ),
		'no_terms'									 => __( 'No countries', 'annobib-plugin' ),
		'items_list_navigation' 		 => __( 'Countries list navigation', 'annobib-plugin' ),
		'items_list'								 => __( 'Countries list', 'annobib-plugin' ),
		'most_used'									 => __( 'Most Used', 'annobib-plugin' ),
		'back_to_items'							 => __( '&larr; Back to Countries of Publication', 'annobib-plugin' )
	);

  // Countries of Publication: configuration
	$args = array(
		'labels'            => $labels,
		'public'            => true,
    'show_in_rest'      => true,
		'show_admin_column' => true,
		'description'       => __( 'Preferably use country codes, and choose multiple countries of publication if necessary.', 'annobib-plugin' ),
  	'hierarchical'      => false,
		'rewrite'           => true
	);

  // Countries of Publication
	register_taxonomy( 'en-country', array( 'bibliography-en' ), $args );
	// register_taxonomy( 'de-country', array( 'bibliography-de' ), $args ); NOT IN USE

  // Adaptation Genres: labels
	$labels = array(
		'name'									=> __( 'Adaptation Genres', 'annobib-plugin' ),
		'singular_name'					=> __( 'Adaptation Genre', 'annobib-plugin' ),
		'search_items'					=> __( 'Search Adaptation Genres', 'annobib-plugin' ),
		'all_items'							=> __( 'All Adaptation Genres', 'annobib-plugin' ),
		'parent_item'						=> __( 'Superordinate Adaptation Genre', 'annobib-plugin' ),
		'parent_item_colon'			=> __( 'Superordinate Adaptation Genre:', 'annobib-plugin' ),
		'edit_item'							=> __( 'Edit Adaptation Genre', 'annobib-plugin' ),
		'view_item'							=> __( 'View Adaptation Genre', 'annobib-plugin' ),
		'update_item'						=> __( 'Update Adaptation Genre', 'annobib-plugin' ),
		'add_new_item'					=> __( 'Add New Adaptation Genre', 'annobib-plugin' ),
		'new_item_name'					=> __( 'New Adaptation Genre Name', 'annobib-plugin' ),
		'not_found'							=> __( 'No adaptation genres found.', 'annobib-plugin' ),
		'no_terms'							=> __( 'No adaptation genres', 'annobib-plugin' ),
		'items_list_navigation' => __( 'Adaptation genres list navigation', 'annobib-plugin' ),
		'items_list'						=> __( 'Adaptation genres list', 'annobib-plugin' ),
		'most_used'							=> __( 'Most Used', 'annobib-plugin' ),
		'back_to_items'					=> __( '&larr; Back to Adaptation Genres', 'annobib-plugin' )
	);

  // Adaptation Genres: configuration
	$args = array(
		'labels'            => $labels,
		'public'            => true,
    'show_in_rest'      => true,
		'show_admin_column' => true,
		'description'       => __( 'Select which types of hypo- and hypertexts of the bibliographic entry exist.', 'annobib-plugin' ),
  	'hierarchical'      => true,
		'rewrite'           => true
	);

  // Adaptation Genres
	register_taxonomy( 'en-adaptation', array( 'bibliography-en' ), $args );
	register_taxonomy( 'de-adaptation', array( 'bibliography-de' ), $args );

	// Curricular Levels: labels
	$labels = array(
		'name'									=> __( 'School Years', 'annobib-plugin' ),
		'singular_name'					=> __( 'School Year', 'annobib-plugin' ),
		'search_items'					=> __( 'Search School Years', 'annobib-plugin' ),
		'all_items'							=> __( 'All School Years', 'annobib-plugin' ),
		'parent_item'						=> __( 'Superordinate Level', 'annobib-plugin' ),
		'parent_item_colon'			=> __( 'Superordinate Level:', 'annobib-plugin' ),
		'edit_item'							=> __( 'Edit School Year', 'annobib-plugin' ),
		'view_item'							=> __( 'View School Year', 'annobib-plugin' ),
		'update_item'						=> __( 'Update School Year', 'annobib-plugin' ),
		'add_new_item'					=> __( 'Add New School Year', 'annobib-plugin' ),
		'new_item_name'					=> __( 'New School Year Name', 'annobib-plugin' ),
		'not_found'							=> __( 'No school years found.', 'annobib-plugin' ),
		'no_terms'							=> __( 'No school years', 'annobib-plugin' ),
		'items_list_navigation' => __( 'School year list navigation', 'annobib-plugin' ),
		'items_list'						=> __( 'School year list', 'annobib-plugin' ),
		'most_used'							=> __( 'Most Used', 'annobib-plugin' ),
		'back_to_items'					=> __( '&larr; Back to School Years', 'annobib-plugin' )
	);

  // Curricular Levels: configuration
	$args = array(
		'labels'            => $labels,
		'public'            => true,
    'show_in_rest'      => true,
		'show_admin_column' => true,
		'description'       => __( 'Curricular levels can, for example, be organised by type of school and school year.', 'annobib-plugin' ),
  	'hierarchical'      => true,
		'rewrite'           => true
	);

  // Curricular Levels
	register_taxonomy( 'en-curriculum', array( 'bibliography-en' ), $args );
	register_taxonomy( 'de-curriculum', array( 'bibliography-de' ), $args );

	// Topics: labels
	$labels = array(
		'name'									=> __( 'Topics', 'annobib-plugin' ),
		'singular_name'					=> __( 'Topic', 'annobib-plugin' ),
		'search_items'					=> __( 'Search Topics', 'annobib-plugin' ),
		'all_items'							=> __( 'All Topics', 'annobib-plugin' ),
		'parent_item'						=> __( 'Parent Topic', 'annobib-plugin' ),
		'parent_item_colon'			=> __( 'Parent Topic:', 'annobib-plugin' ),
		'edit_item'							=> __( 'Edit Topic', 'annobib-plugin' ),
		'view_item'							=> __( 'View Topic', 'annobib-plugin' ),
		'update_item'						=> __( 'Update Topic', 'annobib-plugin' ),
		'add_new_item'					=> __( 'Add New Topic', 'annobib-plugin' ),
		'new_item_name'					=> __( 'New Topic Name', 'annobib-plugin' ),
		'not_found'							=> __( 'No topics found.', 'annobib-plugin' ),
		'no_terms'							=> __( 'No topics', 'annobib-plugin' ),
		'items_list_navigation'	=> __( 'Topics list navigation', 'annobib-plugin' ),
		'items_list'						=> __( 'Topics list', 'annobib-plugin' ),
		'most_used'							=> __( 'Most Used', 'annobib-plugin' ),
		'back_to_items'					=> __( '&larr; Back to Topics', 'annobib-plugin' )
	);

  // Topics: configuration
	$args = array(
		'labels'            => $labels,
		'public'            => true,
    'show_in_rest'      => true,
		'show_admin_column' => true,
		'description'       => __( 'Identify relevant topics across the curricula to be covered by the bibliography.', 'annobib-plugin' ),
  	'hierarchical'      => true,
		'rewrite'           => true
	);

  // Topics
	register_taxonomy( 'en-topic', array( 'bibliography-en' ), $args );
	register_taxonomy( 'de-topic', array( 'bibliography-de' ), $args );

	// Contexts: labels
	$labels = array(
		'name'									=> __( 'Contexts', 'annobib-plugin' ),
		'singular_name'					=> __( 'Context', 'annobib-plugin' ),
		'search_items'					=> __( 'Search Contexts', 'annobib-plugin' ),
		'all_items'							=> __( 'All Contexts', 'annobib-plugin' ),
		'parent_item'						=> __( 'Parent Context', 'annobib-plugin' ),
		'parent_item_colon'			=> __( 'Parent Context:', 'annobib-plugin' ),
		'edit_item'							=> __( 'Edit Context', 'annobib-plugin' ),
		'view_item'							=> __( 'View Context', 'annobib-plugin' ),
		'update_item'						=> __( 'Update Context', 'annobib-plugin' ),
		'add_new_item'					=> __( 'Add New Context', 'annobib-plugin' ),
		'new_item_name'					=> __( 'New Context Name', 'annobib-plugin' ),
		'not_found'							=> __( 'No contexts found.', 'annobib-plugin' ),
		'no_terms'							=> __( 'No contexts', 'annobib-plugin' ),
		'items_list_navigation'	=> __( 'Contexts list navigation', 'annobib-plugin' ),
		'items_list'						=> __( 'Contexts list', 'annobib-plugin' ),
		'most_used'							=> __( 'Most Used', 'annobib-plugin' ),
		'back_to_items'					=> __( '&larr; Back to Contexts', 'annobib-plugin' )
	);

  // Contexts: configuration
	$args = array(
		'labels'            => $labels,
		'public'            => true,
    'show_in_rest'      => true,
		'show_admin_column' => true,
		'description'       => __( 'Identify important contexts in which bibliographic entries produce meaning.', 'annobib-plugin' ),
  	'hierarchical'      => true,
		'rewrite'           => true
	);

  // Contexts
	// register_taxonomy( 'en-context', array( 'bibliography-en' ), $args ); NOT IN USE
	register_taxonomy( 'de-context', array( 'bibliography-de' ), $args );

	// Canonisation Criteria: labels
	$labels = array(
		'name'									=> __( 'Arguments in Favour', 'annobib-plugin' ),
		'singular_name'					=> __( 'Argument', 'annobib-plugin' ),
		'search_items'					=> __( 'Search Arguments', 'annobib-plugin' ),
		'all_items'							=> __( 'All Arguments', 'annobib-plugin' ),
		'parent_item'						=> __( 'Superordinate Argument', 'annobib-plugin' ),
		'parent_item_colon'			=> __( 'Superordinate Argument:', 'annobib-plugin' ),
		'edit_item'							=> __( 'Edit Argument', 'annobib-plugin' ),
		'view_item'							=> __( 'View Argument', 'annobib-plugin' ),
		'update_item'						=> __( 'Update Argument', 'annobib-plugin' ),
		'add_new_item'					=> __( 'Add New Argument', 'annobib-plugin' ),
		'new_item_name'					=> __( 'New Argument Name', 'annobib-plugin' ),
		'not_found'							=> __( 'No arguments in favour found.', 'annobib-plugin' ),
		'no_terms'							=> __( 'No arguments in favour', 'annobib-plugin' ),
		'items_list_navigation' => __( 'Arguments list navigation', 'annobib-plugin' ),
		'items_list'						=> __( 'Arguments list', 'annobib-plugin' ),
		'most_used'							=> __( 'Most Used', 'annobib-plugin' ),
		'back_to_items'					=> __( '&larr; Back to Arguments in Favour', 'annobib-plugin' )
	);

  // Canonisation Criteria: configuration
	$args = array(
		'labels'            => $labels,
		'public'            => true,
    'show_in_rest'      => true,
		'show_admin_column' => true,
		'description'       => __( 'Arguments in favour of the canonisation of a given entry should be carefully considered and made transparent to users.', 'annobib-plugin' ),
  	'hierarchical'      => true,
		'rewrite'           => true
	);

  // Canonisation Criteria
	register_taxonomy( 'en-criterion', array( 'bibliography-en' ), $args );
	register_taxonomy( 'de-criterion', array( 'bibliography-de' ), $args );
}
add_action( 'init', 'annobib_setup_custom_taxonomy' );


// REGISTER THE REQUIRED META FIELDS
function annobib_setup_post_meta() {

	// Citation: configuration
	$args = array(
		'type'					=> 'string',
		'description'		=> __( 'Paragraph containing a standard citation of the recommended edition.', 'annobib-plugin' ),
		'single'				=> true,
		'show_in_rest'	=> true,
		'auth_callback'	=> function() {
			return current_user_can( 'edit_posts' );
		}
	);

	// Citation
	register_post_meta( 'bibliography-en', 'annobib-meta-citation', $args );
	register_post_meta( 'bibliography-de', 'annobib-meta-citation', $args );

	// Unique Identifier (Type): configuration
	$args = array(
		'type'					=> 'string',
		'description'		=> __( 'ID type for the recommended edition, e.g. ISBN.', 'annobib-plugin' ),
		'single'				=> true,
		'show_in_rest'	=> true,
		'auth_callback'	=> function() {
			return current_user_can( 'edit_posts' );
		}
	);

	// Unique Identifier (Type)
	register_post_meta( 'bibliography-en', 'annobib-meta-uniqueid-type', $args );
	register_post_meta( 'bibliography-de', 'annobib-meta-uniqueid-type', $args );

	// Unique Identifier (ID): configuration
	$args = array(
		'type'					=> 'string',
		'description'		=> __( 'Unique identifier for the recommended edition, e.g. 9780199536450.', 'annobib-plugin' ),
		'single'				=> true,
		'show_in_rest'	=> true,
		'auth_callback'	=> function() {
			return current_user_can( 'edit_posts' );
		}
	);

	// Unique Identifier (ID)
	register_post_meta( 'bibliography-en', 'annobib-meta-uniqueid-id', $args );
	register_post_meta( 'bibliography-de', 'annobib-meta-uniqueid-id', $args );

	// Content Rating: configuration
	$args = array(
		'type'					=> 'string',
		'description'		=> __( 'Official content rating in Germany, e.g. FSK 12.', 'annobib-plugin' ),
		'single'				=> true,
		'show_in_rest'	=> true,
		'auth_callback'	=> function() {
			return current_user_can( 'edit_posts' );
		}
	);

	// Content Rating
	register_post_meta( 'bibliography-en', 'annobib-meta-contentrating', $args );
	register_post_meta( 'bibliography-de', 'annobib-meta-contentrating', $args );

	// Length (Amount): configuration
	$args = array(
		'type'					=> 'integer',
		'description'		=> __( 'Length of the recommended edition, e.g. 143.', 'annobib-plugin' ),
		'single'				=> true,
		'show_in_rest'	=> true,
		'auth_callback'	=> function() {
			return current_user_can( 'edit_posts' );
		}
	);

	// Length (Amount)
	register_post_meta( 'bibliography-en', 'annobib-meta-length-amount', $args );
	register_post_meta( 'bibliography-de', 'annobib-meta-length-amount', $args );

	// Length (Unit): configuration
	$args = array(
		'type'					=> 'string',
		'description'		=> __( 'Unit that the length of the recommended edition is measured in, e.g. pages.', 'annobib-plugin' ),
		'single'				=> true,
		'show_in_rest'	=> true,
		'auth_callback'	=> function() {
			return current_user_can( 'edit_posts' );
		}
	);

	// Length (Unit)
	register_post_meta( 'bibliography-en', 'annobib-meta-length-unit', $args );
	register_post_meta( 'bibliography-de', 'annobib-meta-length-unit', $args );

	// Useful Links: configuration
	$args = array(
		'type'					=> 'string',
		'description'		=> __( 'List of useful links, e.g. a text in the public domain or additional information.', 'annobib-plugin' ),
		'single'				=> true,
		'show_in_rest'	=> true,
		'auth_callback'	=> function() {
			return current_user_can( 'edit_posts' );
		}
	);

	// Useful Links
	register_post_meta( 'bibliography-en', 'annobib-meta-usefullinks', $args );
	register_post_meta( 'bibliography-de', 'annobib-meta-usefullinks', $args );

	// Year of First Publication: configuration
	$args = array(
		'type'					=> 'integer',
		'description'		=> __( 'Year in which the text was first published.', 'annobib-plugin' ),
		'single'				=> true,
		'show_in_rest'	=> true,
		'auth_callback'	=> function() {
			return current_user_can( 'edit_posts' );
		}
	);

	// Year of First Publication
	register_post_meta( 'bibliography-en', 'annobib-meta-year', $args );
	register_post_meta( 'bibliography-de', 'annobib-meta-year', $args );

	// Year of First Publication (Guess): configuration
	$args = array(
		'type'					=> 'boolean',
		'description'		=> __( 'Binary field saying whether the year is a best guess.', 'annobib-plugin' ),
		'single'				=> true,
		'show_in_rest'	=> true,
		'auth_callback'	=> function() {
			return current_user_can( 'edit_posts' );
		}
	);

	// Year of First Publication (Guess)
	register_post_meta( 'bibliography-en', 'annobib-meta-year-guess', $args );
	register_post_meta( 'bibliography-de', 'annobib-meta-year-guess', $args );

	// Notes for Editors: configuration
	$args = array(
		'type'					=> 'string',
		'description'		=> __( 'Unpublished field for what authors and editors need to know about this entry to make informed decisions.', 'annobib-plugin' ),
		'single'				=> true,
		'show_in_rest'	=> true,
		'auth_callback'	=> function() {
			return current_user_can( 'edit_posts' );
		}
	);

	// Notes for Editors
	register_post_meta( 'bibliography-en', 'annobib-meta-notes', $args );
	register_post_meta( 'bibliography-de', 'annobib-meta-notes', $args );

	// Featured: configuration
	$args = array(
		'type'					=> 'boolean',
		'description'		=> __( 'Binary field to indicate whether an entry should be featured on the front page.', 'annobib-plugin' ),
		'single'				=> true,
		'show_in_rest'	=> true,
		'auth_callback'	=> function() {
			return current_user_can( 'edit_posts' );
		}
	);

	// Featured
	register_post_meta( 'bibliography-en', 'annobib-meta-featured', $args );
	register_post_meta( 'bibliography-de', 'annobib-meta-featured', $args );
}
add_action( 'init', 'annobib_setup_post_meta' );


// ADD NEW BLOCK CATEGORY FOR CUSTOM BLOCKS
function annobib_setup_block_categories( $categories, $post ) {

	// The bibliography post types get a new block category
	if ( $post->post_type == 'bibliography-en' OR $post->post_type == 'bibliography-de' ) {
		return array_merge(
			$categories,
			array(
				array(
					'slug'  => 'annobib',
					'title' => __( 'Annotated Bibliography', 'annobib-plugin' ),
					'icon'  => 'dashicons-book'
				),
			)
		);
	}

	// Regular post types do not get this category
	else {
		return $categories;
	}
}
add_filter( 'block_categories', 'annobib_setup_block_categories', 10, 2 );


// ENQUEUE BLOCK EDITOR ASSETS; JAVASCRIPT FILE THAT TURNS META FIELDS INTO BLOCKS
function annobib_setup_blocks() {

	// JavaScript file that turns the meta fields into blocks
	wp_enqueue_script(
		'annobib-blocks',
		plugins_url( 'annobib-blocks.js', __FILE__ ),
		array(
			'wp-blocks',
			'wp-element',
			'wp-components',
			'wp-i18n',
			'wp-editor'
		)
	);

	// Translations for the JavaScript file
	wp_set_script_translations( 'annobib-blocks', 'annobib-plugin', plugin_dir_path( __FILE__ ) . 'languages' );

	// CSS file which corrects some misalignments in the block editor
	wp_enqueue_style(
		'annobib-styles',
		plugins_url( 'annobib-styles.css', __FILE__ ),
		array()
	);
}
add_action( 'enqueue_block_editor_assets', 'annobib_setup_blocks' );


// ADD A SUPER AUTHOR ROLE WITH MORE CAPABILITIES THAN REGULAR AUTHORS
function annobib_setup_roles() {

	// Add a superauthor role
	add_role(
		'super_author',
		__( 'Super Author', 'annobib-plugin' ),
		[ 'read'									 => true,
			'read_private_pages'		 => true,
			'read_private_posts'		 => true,
			'edit_pages'						 => true,
			'edit_posts'						 => true,
			'edit_others_pages'			 => true,
			'edit_others_posts'			 => true,
			'edit_private_pages'		 => true,
			'edit_private_posts'		 => true,
			'edit_published_pages'	 => true,
			'edit_published_posts'	 => true,
			'delete_pages'					 => true,
			'delete_posts'					 => true,
			'delete_others_pages'		 => true,
			'delete_others_posts'		 => true,
			'delete_private_pages'	 => true,
			'delete_private_posts'	 => true,
			'delete_published_pages' => true,
			'delete_published_posts' => true,
			'upload_files'					 => true,
			'manage_links'					 => true,
			'manage_categories'			 => true
			// Regular authors also have 'publish_posts'
			// Regular editors also have 'unfiltered_html', 'moderate_comments', 'publish_pages', 'publish_posts'
		] );
}

?>
