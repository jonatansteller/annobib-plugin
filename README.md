# Annotated Bibliography Plugin

Plugin to enable WordPress to host an annotated bibliography if accompanied by the theme of the same name. Both are being developed by staff of the Zentrum für Lehrerbildung und Schulforschung at Leipzig University. The plugin powers a project called [Lit4School](https://home.uni-leipzig.de/lit4school/).

- *Current version:* 0.9
- *Release date:* 18 February 2020

## Features

- Creates two custom post types for bibliographic entries on two school subjects (English and German)
- Adds taxonomies to this post type: genres, authors, countries of publication (for English), topics, contexts (for German), adaptation genres, curricular data, and canonisation criteria
- Adds meta fields to this post type: year of first publication, citation (RichText), unique identifier (2x), content rating, length (2x), useful links (RichText), notes for editors
- Adds 'meta boxes' to the Gutenberg editor to edit the above meta fields
- Adds a 'super author' role to include more abilities than regular authors
- Has preliminary activation and deactivation functions to preserve data in the database
- Translated to German

## Roadmap

- Evaluate whether parts of the optional second paragraph might make more sense in an open/moderated discussion section below the entry.

## Contributing

If you encounter a bug in the backend layout or the database structure this plugin produces, feel free to use the bug tracker on the [plugin's GitLab page](https://gitlab.com/jonatansteller/annobib-plugin/issues).

For frontend bugs please refer to the [theme's GitLab page](https://gitlab.com/jonatansteller/annobib-theme/issues) instead.
